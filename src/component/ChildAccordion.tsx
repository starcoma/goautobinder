import React from 'react';
import { Checkbox, makeStyles, Grid, RadioGroup, Radio, FormControlLabel } from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TableContainer from '@material-ui/core/TableContainer';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: '0',
    },
    heading: {
        textAlign: 'left',
        fontSize: theme.typography.pxToRem(20),
    },
    subHeading: {
        textAlign: 'left',
        fontSize: theme.typography.pxToRem(12),
    },
    description: {
        textAlign: 'left',
        fontSize: theme.typography.pxToRem(14),
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
    icon: {
        verticalAlign: 'bottom',
        height: 20,
        width: 20,
    },
    details: {
        alignItems: 'center',
    },
    column: {
        flexBasis: '100%',
        display: 'flex'
    },
    helper: {
        borderLeft: `2px solid ${theme.palette.divider}`,
        padding: theme.spacing(1, 2),
    },
    link: {
        color: theme.palette.primary.main,
        textDecoration: 'none',
        '&:hover': {
            textDecoration: 'underline',
        },
    },
    txtMargin: {
        marginLeft: '10px'
    }
}));

interface IChildAccordionProps {
    children: React.ReactElement;
    title: string;
    subtitle: string;
    description: string;
    descriptionTable?: string;
    hasCheckBox: boolean;
    isDisabled: boolean;
    isChecked: boolean;
    hasRadioGroup?: boolean;
}

export default React.memo(function ChildAccordion(props: IChildAccordionProps) {
    const classes = useStyles();
    const [state, setState] = React.useState({
        isAccept: false
    });

    const handleAcceptReject = (value: string) => {
        console.log(value);
        if (value === 'accept')
            setState({ ...state, isAccept: true });
        else
            setState({ ...state, isAccept: false });
    }

    return (
        <div className={classes.root}>
            <Accordion defaultExpanded>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1c-content"
                    id={props.title}
                >
                    <div className={classes.column}>
                        <Grid container spacing={3}>
                            <Grid item xs={12} sm={6} className={classes.column}>
                                {props.hasCheckBox && props.isDisabled && <Checkbox color="primary" name="saveAddress" value="yes" disabled={props.isDisabled} checked={true} />}
                                {props.hasCheckBox && !props.isDisabled && <Checkbox color="primary" name="saveAddress" value="yes" disabled={props.isDisabled} />}
                                <div>
                                    <Typography className={classes.heading}>{props.title}</Typography>
                                    <Typography className={classes.subHeading}>{props.subtitle}</Typography>
                                </div>
                            </Grid>
                            <Grid item xs={12} sm={6} className={classes.column}>
                                {props.hasRadioGroup &&
                                    <>
                                        <RadioGroup id={"uninsured_motorist_coverage"} row defaultValue={"reject"} onChange={(e: React.ChangeEvent<HTMLInputElement>, value: string) => {
                                            handleAcceptReject(value);
                                        }}>
                                            <FormControlLabel key={"accept"} value={"accept"} control={<Radio color="primary" />} label="Accept" />
                                            <FormControlLabel key={"reject"} value={"reject"} control={<Radio color="primary" />} label="Reject" />
                                        </RadioGroup>
                                    {state.isAccept && <FormControlLabel className={classes.txtMargin} control={<span></span>} label="___________(initials)."/>}
                                    {!state.isAccept && <FormControlLabel className={classes.txtMargin} control={<span></span>} label="F.L. (initials)"/>}
                                    </>
                                }

                            </Grid>
                        </Grid>
                    </div>
                </AccordionSummary>
                <AccordionDetails className={classes.details}>
                    <Typography className={classes.description}>{props.description}  </Typography>
                </AccordionDetails>
                <AccordionDetails className={classes.details}>
                    <TableContainer >
                        <Table size="small" aria-label="a dense table">
                            <TableBody>
                                {props.descriptionTable && props.descriptionTable.split('.').map((row: any) => (
                                    <TableRow key={row}>
                                        <TableCell component="th" scope="row">
                                            {row}
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </AccordionDetails>
            </Accordion>
        </div>
    );
})