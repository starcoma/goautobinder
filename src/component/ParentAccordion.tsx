import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: '10px auto 0 auto',
        boxShadow:'0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12)',
        borderRadius:'5px'
    },
    heading: {
        paddingLeft: '10px',
        textAlign: 'left',
        fontSize: theme.typography.pxToRem(20),
        background: 'linear-gradient(to right,yellow, white)',
        color: 'grey',
        width: '300px',
        "@media (max-width: 760px)": {
            width: '100%'   
        }
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
    icon: {
        verticalAlign: 'bottom',
        height: 20,
        width: 20,
    },
    details: {
        alignItems: 'center',
    },
    column: {
        flexBasis: '100%',
    },
    helper: {
        borderLeft: `2px solid ${theme.palette.divider}`,
        padding: theme.spacing(1, 2),
    },
    link: {
        color: theme.palette.primary.main,
        textDecoration: 'none',
        '&:hover': {
            textDecoration: 'underline',
        },
    },
}));

interface IParentAccordionProps {
    children: React.ReactElement;
    title: string;
}
export default React.memo(function DetailedAccordion(props: IParentAccordionProps) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Accordion defaultExpanded>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1c-content"
                    id="panel1c-header"
                >
                    <div className={classes.column}>
                        <Typography className={classes.heading}>{props.title}</Typography>
                    </div>
                </AccordionSummary>
                <AccordionDetails className={classes.details}>
                    {props.children}
                </AccordionDetails>
            </Accordion>
        </div>
    );
})