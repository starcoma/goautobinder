import React from 'react';
import { Drawer, Hidden, List, Link, ListItem, ListItemIcon, ListItemText, makeStyles, useTheme, Theme, createStyles } from '@material-ui/core';
import ContactsIcon from '@material-ui/icons/Contacts';
import ListIcon from '@material-ui/icons/List';
import LogoImg from '../assets/img/Go.png';

const drawerWidth = 240;
const drawerHeight = 600;
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            
        },
        drawer: {
            [theme.breakpoints.up('sm')]: {
                width: drawerWidth,
                flexShrink: 0
            },
        },
        appBar: {
            [theme.breakpoints.up('sm')]: {
                width: `calc(100% - ${drawerWidth}px)`
            },
        },
        menuButton: {
            marginRight: theme.spacing(2),
            [theme.breakpoints.up('sm')]: {
                display: 'none',
            },
        },
        // necessary for content to be below app bar
        toolbar: theme.mixins.toolbar,
        drawerPaper: {
            width: drawerWidth,
            height: drawerHeight,
            borderRadius: '5px',
            left: 'unset',
            marginTop: '10px',
            boxShadow:'0 2px 1px -1px rgba(0,0,0,.2), 0 1px 1px 0 rgba(0,0,0,.14), 0 1px 3px 0 rgba(0,0,0,.12)',
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
        },
        large: {
            width: '100%',
            height: '100%',
            marginTop:'-10px'
        },
        subTitle: {
            '& p': {
                display: 'inline-grid',
                fontSize: '12px',
                '& a': {
                    color: 'grey',
                    textDecoration: 'underline'
                }
            }
        }
    })
);

interface Props {
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window?: () => Window;
}

export default React.memo(function SideBar(props: Props) {
    const { window } = props;
    const classes = useStyles();
    const theme = useTheme();
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const drawer = (
        <div>
            {/* <div className={classes.toolbar} /> */}
           {/*  <Divider /> */}
            <List>
                    <img className={classes.large} alt="Sharp" src={LogoImg} />
                {['FORMS', 'CONTACTS'].map((text, index) => (
                    <ListItem button key={text}>
                        <ListItemIcon>{index % 2 === 0 ? <ListIcon /> : <ContactsIcon />}</ListItemIcon>
                        <ListItemText className={classes.subTitle} primary={text} secondary={index % 2 !== 0 ? "Service (855) 658-5775 Marketing (480) 433-4368" : <><Link href="#">Unwind or Rewind</Link><Link href="#">Flat Cancel</Link></>}/>
                    </ListItem>
                ))}
            </List>
        </div>
    );

    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        <>
            <nav className={classes.drawer} aria-label="mailbox folders">
                <Hidden smUp implementation="css">
                    <Drawer
                        container={container}
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                    <Drawer
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        variant="permanent"
                        open
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
            </nav>
        </>
    );
})
