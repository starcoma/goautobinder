import * as React from 'react';
import { IErrors, FormContext, IValues } from './Form';
import { TextField, FormControl, FormControlLabel, FormLabel, RadioGroup, Radio, makeStyles, Checkbox } from '@material-ui/core';
/* The available editors for the field */
type Editor = 'textbox' | 'multilinetextbox' | 'dropdown' | 'date' | 'button' | 'radiogroup' | 'checkbox';

export interface IValidation {
    rule: (values: IValues, fieldName: string, args: any) => string;
    args?: any;
}

export interface IFieldProps {
    /* The unique field name */
    id: string;

    /* The label text for the field */
    label?: string;

    /* The placeholder text for the field */
    placeholder?: string;

    /* The editor for the field */
    editor?: Editor;

    /* The drop down items for the field */
    options?: string[];

    /* The field value */
    value?: any;

    /* The field validator function and argument */
    validation?: IValidation;
}

const useStyles = makeStyles((theme) => ({
    formControl: {
        width: '100%',
        textAlign: 'left',
        '& .form-group': {
            margin: '0 !important'
        }
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    button: {
        margin: theme.spacing(1),
    },
}));

export const Field: React.SFC<IFieldProps> = React.memo(({
    id,
    label,
    placeholder,
    editor,
    options,
    value
}) => {

    const classes = useStyles();

    /**
     * Gets the validation error for the field
     * @param {IErrors} errors - All the errors from the form
     * @returns {string[]} - The validation error
     */
    const getError = (errors: IErrors): string => (errors ? errors[id] : "");

    /**
     * Gets the inline styles for editor
     * @param {IErrors} errors - All the errors from the form
     * @returns {any} - The style object
     */
    const getEditorStyle = (errors: IErrors): any =>
        getError(errors) ? { borderColor: "#e01933" } : {};

    return (
        <FormContext.Consumer>
            {(context: any) => (
                <div className={classes.formControl}>
                    {editor!.toLowerCase() === "textbox" && (
                        <TextField
                            id={id}
                            label={label}
                            // value={value}
                            placeholder={placeholder}
                            InputLabelProps={{ shrink: true, required: true }}
                            variant="outlined"
                            margin="dense"
                            style={context && getEditorStyle(context.errors)}
                            onChange={
                                (e: any) => {
                                    context.setValues({ [id]: e.target.value });
                                }
                            }
                            onBlur={() => context && context.validate(id)}
                            fullWidth
                            autoComplete="given-name"
                        />
                    )}
                    {editor!.toLowerCase() === "multilinetextbox" && (
                        <TextField
                            id={id}
                            label={label}
                            multiline
                            rowsMax={4}
                            InputLabelProps={{ shrink: true, required: true }}
                            value={value}
                            style={context && getEditorStyle(context.errors)}
                            onChange={
                                (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => 
                                    context && context.setValues({ [id]: e.target.value })
                            }
                            onBlur={() => context && context.validate(id)}
                            fullWidth
                            autoComplete="given-name"
                        />
                    )}
                    {editor!.toLowerCase() === "date" && (
                        <TextField
                            id={id}
                            label={label}
                            type="date"
                            variant="outlined"
                            margin="dense"
                            value={value}
                            InputLabelProps={{ shrink: true, required: true }}
                            style={context && getEditorStyle(context.errors)}
                            onChange={
                                (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => 
                                    context && context.setValues({ [id]: e.target.value })
                            }
                            onBlur={() => context && context.validate(id)}
                            fullWidth
                            helperText={placeholder}
                        />
                    )}
                    {editor!.toLowerCase() === "radiogroup" && (
                        <>
                            <FormLabel component="legend">{label}</FormLabel>
                            <RadioGroup id={id} row name={label} onChange={(e: React.ChangeEvent<HTMLInputElement>, value: string) => {
                                context && context.setValues({ [id]: value })
                            }}>
                                {options &&
                                    options.map(option => (
                                        <FormControlLabel key={option} value={option.toLowerCase()} control={<Radio color="primary" />} label={option} />
                                    ))}
                            </RadioGroup>
                        </>
                    )}
                    {editor!.toLowerCase() === "checkbox" && (
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {  }}
                                    color="primary"
                                />
                            }
                            label={label}
                        />
                    )}
                    {editor!.toLowerCase() === "dropdown" && (
                        <FormControl className={classes.formControl}>
                            {/* <InputLabel id="demo-simple-select-autowidth-label">{label}</InputLabel> */}
                            <TextField
                                select
                                id={id}
                                variant="outlined"
                                style={context && getEditorStyle(context.errors)}
                                margin="dense"
                                onChange={
                                    (e: React.ChangeEvent<{
                                        name?: string | undefined;
                                        value: unknown;
                                    }>) => 
                                        context && context.setValues({ [id]: e.target.value })
                                }
                                onBlur={() => context && context.validate(id)}
                                fullWidth
                                SelectProps={{
                                    native: true,
                                }}

                            >
                                {options &&
                                    options.map(option => (
                                        <option key={option} value={option}>
                                            {option}
                                        </option>
                                    ))}
                            </TextField>
                        </FormControl>
                    )}

                    {context && getError(context.errors) && (
                        <div style={{ color: "#e01933", fontSize: "13px", textAlign: "left" }}>
                            <p style={{ margin: '0' }}>{getError(context.errors)}</p>
                        </div>
                    )}
                </div>
            )}
        </FormContext.Consumer>
    );
});
Field.defaultProps = {
    editor: "textbox"
};