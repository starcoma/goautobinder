import React from 'react';
import { Typography } from '@material-ui/core';

export default function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
                2017- {new Date().getFullYear()}
            {'.'} <br /> Commonwealth Casualty Company. <br />All Rights Reserved.
        </Typography>
    );
}