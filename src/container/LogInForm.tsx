import React from 'react';
import { history } from "../history";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import LogoImg from '../assets/img/Go.png';
import { CssBaseline } from '@material-ui/core';
import { Paper } from '@material-ui/core';
import Copyright from '../component/Copyright'

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        width: 'fit-content',
        margin: 'auto'
    },
    main: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        marginTop: '20vh',
        borderRadius: '15px',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '30rem',
    },
    avatar: {
        marginTop: '-7rem',
        marginBottom: '1rem',
        width: '7rem',
        height: '7rem',
        borderRadius: '10px'
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        width: '13rem',
        marginLeft: '8.5rem',
    },
    forgot: {
        textAlign: 'center'
    }
}));

export default function SignInSide() {
    const classes = useStyles();
    /**
     * Handles form submission
     * @param {React.FormEvent<HTMLFormElement>} e - The form event
     */
    const handleSubmit = async (
        e: React.FormEvent<HTMLFormElement>
    ): Promise<void> => {
        e.preventDefault();
        history.push('/goUI');
    };
    return (
        <div className={classes.root}>
        <Grid container component="main" className={classes.main}>
            <CssBaseline />
            <Grid item xs={12} sm={12} md={12} component={Paper} elevation={5} >
                <div className={classes.paper}>
                    <img className={classes.avatar} src={LogoImg} alt="logo" />
                    <Typography component="h1" variant="h5">
                        Log In
                        </Typography>
                    <form className={classes.form} onSubmit={handleSubmit} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="dealer_id"
                            label="Dealer ID"
                            name="dealer_id"
                            autoComplete="dealer_id"
                            autoFocus
                        />
                        <Button
                            size="large"
                            type="submit"
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Log In
                        </Button>
                        <Grid className={classes.forgot}>
                            <Grid item xs >
                                <Link href="/restore" variant="body1" >
                                    Forgot Dealer ID?
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                    <Box mt={5}>
                        <Copyright />
                    </Box>
                </div>
            </Grid>
        </Grid>
        </div>
    );
}