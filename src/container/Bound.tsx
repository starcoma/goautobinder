import React from 'react';
import { Grid, Button, makeStyles, Card, CardContent, Typography } from '@material-ui/core';
import SideBar from '../component/Sidebar';
import Copyright from '../component/Copyright'

const useStyles = makeStyles({
    root: {
        display: 'flex',
        marginTop: '5vh',
        width: 'fit-content',
        margin: 'auto',
        // minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 18,
        textAlign: 'center',
        marginBottom: '3rem',
    },
    pos: {
        marginBottom: 12,
    },
    align: {
        float: 'right'
    },
    policy: {
        color: 'red',
        textAlign: 'center',
        margin: '1rem 1rem',
    }
});

export default React.memo(function Bound() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid item xs={12} sm={4} style={{ display: 'contents', margin: 'auto' }}>
                <SideBar></SideBar>
            </Grid>
            <Grid item xs={12} sm={8}>
            <p style={{ textAlign: 'right', marginRight: '15px' }}> <a href="/">Logout</a></p>
                <Card className={classes.root}>
                    <CardContent>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Your Information
                        </Typography>

                        <Typography gutterBottom variant="h6" component="h2">
                            Your policy is:
                        </Typography>
                        <hr />
                        <Typography gutterBottom variant="h6" component="h2">
                            Policy
                        </Typography>
                        <hr />
                        <Typography gutterBottom variant="h6" component="h2">
                            Confirmation
                        </Typography>
                        <hr />
                        <Typography className={classes.policy} gutterBottom variant="h6" component="h2">
                            Policy ID Card and Documents has been emailed to you
                        </Typography>
                        <Typography className={classes.title} gutterBottom variant="body2" component="p">
                            (The policy documents have been emailed to your email address provided to us)
                        </Typography>
                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.align}
                        >
                            Get Documents
                            </Button>
                    </CardContent>
                </Card>
                <Copyright/>
            </Grid>
        </div>
    );
})