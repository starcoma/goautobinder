import * as React from 'react';
import { Grid, Button, Collapse, FormControlLabel, FormControl, TextField, Checkbox, makeStyles, Theme, FormHelperText, Link } from '@material-ui/core';
import { Form, IFields, required, isEmail, maxLength } from '../component/Form';
import ParentAccordion from '../component/ParentAccordion';
import ChildAccordion from '../component/ChildAccordion';
import { Field } from '../component/Field';
import SideBar from '../component/Sidebar';
import Copyright from '../component/Copyright'



const useStyles = makeStyles((theme: Theme) => ({
    root: {
        display: 'flex',
        width: 'fit-content',
        margin: 'auto'
    },
    gridControl: {
        padding: '0 12px 0 12px !important'
    },
    button: {
        float: 'right',
        margin: '0',
    },
    align: {
        float: 'right'
    },
    total: {
        fontSize: '20px',
        fontWeight: 'bold',
        float: 'right',
        color: 'black',
        display: 'contents'
    },
    text: {
        fontSize: '14px',
        color: 'rgba(0, 0, 0, 0.87)',
        lineHeight: '1.2rem'
    },
    formControl: {
        width: '100%',
        textAlign: 'left',
        '& .form-group': {
            margin: '0 !important'
        }
    },
}));

const fields: IFields = {
    vin: {
        id: 'vin',
        label: 'VIN#',
        editor: "textbox",
        placeholder: 'maximum 17 characters',
        validation: { rule: required }
    },
    make: {
        id: 'make',
        label: 'Make',
        placeholder: 'ex: Toyota',
        validation: { rule: required }
    },
    model: {
        id: 'model',
        label: 'Model',
        placeholder: 'ex: Prius',
        validation: { rule: required }
    },
    year: {
        id: 'year',
        label: 'Year',
        placeholder: '',
        validation: { rule: required }
    },
    lienholder_name: {
        id: 'lienholder_name',
        label: 'Lienholder name',
        placeholder: '',
        validation: { rule: required }
    },
    first: {
        id: 'first',
        label: 'First',
        placeholder: '',
        validation: { rule: required }
    },
    middle: {
        id: 'middle',
        label: 'Middle',
        placeholder: '',
        validation: { rule: required }
    },
    last: {
        id: 'last',
        label: 'Last',
        placeholder: '',
        validation: { rule: required }
    },
    dl: {
        id: 'dl',
        label: 'Driver Licence',
        placeholder: '',
        validation: { rule: required }
    },
    date_of_birth: {
        id: 'date_of_birth',
        label: 'Date of Birth',
        editor: 'date',
        validation: { rule: required }
    },
    name_state: {
        id: 'name_state',
        label: 'State',
        editor: 'dropdown',
        options: ['AZ', 'AK', 'AL', 'AR'],
        /* validation: { rule: required } */
    },
    home_phone: {
        id: 'home_phone',
        label: 'Home Phone',
        placeholder: '',
        validation: { rule: required }
    },
    work_phone: {
        id: 'work_phone',
        label: 'Work Phone',
        placeholder: '',
        validation: { rule: required }
    },
    email: {
        id: 'email',
        label: 'Email',
        placeholder: '',
        validation: { rule: isEmail }
    },
    gender: {
        id: 'gender',
        label: 'Gender',
        editor: 'radiogroup',
        options: ['Male', 'Female'],
        validation: { rule: required }
    },
    marital_status: {
        id: 'marital_status',
        label: 'Marital Status',
        editor: 'radiogroup',
        options: ['Single', 'Married'],
        validation: { rule: required }
    },
    address: {
        id: 'address',
        label: 'Address',
        placeholder: '',
        validation: { rule: required }
    },
    city: {
        id: 'city',
        label: 'City',
        placeholder: '',
        validation: { rule: required }
    },
    garaging_state: {
        id: 'garaging_state',
        label: 'State',
        editor: 'dropdown',
        options: ['AZ', 'AK', 'AL', 'AR'],
        validation: { rule: required }
    },
    zip: {
        id: 'zip',
        label: 'Zip',
        placeholder: '',
        validation: { rule: required }
    },
    payment: {
        id: 'payment',
        label: 'Payment method',
        editor: 'dropdown',
        options: ['Dealer', 'Credit Card'],
        validation: { rule: required }
    },
    certification: {
        id: 'certification',
        label: 'Certification',
        editor: 'checkbox',
        validation: { rule: required }
    },
    notes: {
        id: 'notes',
        label: 'Notes',
        editor: 'multilinetextbox',
        validation: { rule: maxLength, args: 1000 }
    },
    dealer_id: {
        id: 'dealer_id',
        label: 'Dealer ID',
        validation: { rule: required }
    },
    stock: {
        id: 'stock',
        label: 'Stock #',
        validation: { rule: required }
    },
    deal: {
        id: 'deal',
        label: 'Deal #',
        validation: { rule: required }
    },
    d_zip_code: {
        id: 'd_zip_code',
        label: 'Zip Code',
        validation: { rule: required }
    },
    first_name: {
        id: 'first_name',
        label: 'First Name',
        validation: { rule: required }
    },
    last_name: {
        id: 'last_name',
        label: 'Last Name',
        validation: { rule: required }
    },
    credit_card_number: {
        id: 'credit_card_number',
        label: 'Credit Card Number',
        validation: { rule: required }
    },
    cvv: {
        id: 'cvv',
        label: 'CVV',
        validation: { rule: required }
    },
    mm: {
        id: 'mm',
        label: 'MM',
        validation: { rule: required }
    },
    c_yyyy: {
        id: 'c_yyyy',
        label: 'YY / YYYY',
        validation: { rule: required }
    },
    c_zip_code: {
        id: 'c_zip_code',
        label: 'Zip Code',
        validation: { rule: required }
    }
};
const CreditCardForm = () => {
    return (
        <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
                <Field {...fields.first_name} />
            </Grid>
            <Grid item xs={10} sm={6}>
                <Field {...fields.last_name} />
            </Grid>
            <Grid item xs={10} sm={12}>
                <Field {...fields.credit_card_number} />
            </Grid>
            <Grid item xs={4} sm={5}>
                <Field {...fields.cvv} />
            </Grid>
            <Grid item xs={12} sm={4}>
                <Field {...fields.mm} />
            </Grid>
            <Grid item xs={12} sm={4}>
                <Field {...fields.c_yyyy} />
            </Grid>
            <Grid item xs={12} sm={4}>
                <Field {...fields.c_zip_code} />
            </Grid>
        </Grid>
    )
}
const DealerForm = () => {
    return (
        <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
                <Field {...fields.dealer_id} />
            </Grid>
            <Grid item xs={10} sm={6}>
                <Field {...fields.stock} />
            </Grid>
            <Grid item xs={10} sm={12}>
                <Field {...fields.deal} />
            </Grid>
            <Grid item xs={12} sm={4}>
                <Field {...fields.d_zip_code} />
            </Grid>
        </Grid>
    )
}
const MainForm = () => {

    const classes = useStyles();
    const [state, setState] = React.useState({
        isDriver: false,
        isMailing: true,
        buttonTitle: 'Add Additional Driver',
        isCreditCard: false
    });

    const handleAdditionalDriver = (key: string, visible: boolean) => {
        if (visible)
            setState({ ...state, [key]: !visible, 'buttonTitle': "Add Additional Driver" });
        else
            setState({ ...state, [key]: !visible, 'buttonTitle': "Remove Additional Driver" });
    };

    const handleMailingAddress = (key: string, visible: boolean) => {
        setState({ ...state, [key]: !visible });
    };

    const handleCreditCard = (value: any) => {
        if (value === 'credit_card')
            setState({ ...state, isCreditCard: true });
        else
            setState({ ...state, isCreditCard: false });
    }
    return (
        <>
            <ParentAccordion title="Vehicle Information">
                <React.Fragment>
                    <Grid container spacing={3}>
                        <Grid item xs={12} sm={12} className={classes.gridControl}>
                            <Field {...fields.vin} />
                        </Grid>
                        <Grid item xs={10} sm={5}>
                            <Field {...fields.make} />
                        </Grid>
                        <Grid item xs={10} sm={5}>
                            <Field {...fields.model} />
                        </Grid>
                        <Grid item xs={4} sm={2}>
                            <Field {...fields.year} />
                        </Grid>
                        <Grid item xs={12} sm={5}>
                            <Field {...fields.lienholder_name} />
                        </Grid>
                    </Grid>
                </React.Fragment>
            </ParentAccordion>
            <ParentAccordion title="Name Insured Information">
                <React.Fragment>
                    <Grid container spacing={3}>
                        <Grid item xs={12} sm={4}>
                            <Field {...fields.first} />
                        </Grid>
                        <Grid item xs={10} sm={4}>
                            <Field {...fields.middle} />
                        </Grid>
                        <Grid item xs={10} sm={4}>
                            <Field {...fields.last} />
                        </Grid>
                        <Grid item xs={4} sm={4}>
                            <Field {...fields.dl} />
                        </Grid>
                        <Grid item xs={8} sm={4}>
                            <Field {...fields.date_of_birth} />
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <Field {...fields.name_state} />
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <Field {...fields.home_phone} />
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <Field {...fields.work_phone} />
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <Field {...fields.email} />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Field {...fields.gender} />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Field {...fields.marital_status} />
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <Button
                                // name={state.isAdditionalDriver}
                                variant="contained"
                                color="primary"
                                onClick={() => { handleAdditionalDriver('isDriver', state.isDriver) }}
                                className={classes.button}
                            >
                                {state.buttonTitle}
                            </Button>
                        </Grid>
                    </Grid>
                </React.Fragment>
            </ParentAccordion>
            <Collapse in={state.isDriver}>
                <ParentAccordion title="Additional Driver">
                    <React.Fragment>
                        <Grid container spacing={3}>
                            <Grid item xs={12} sm={4}>
                                <Field {...fields.first} />
                            </Grid>
                            <Grid item xs={10} sm={4}>
                                <Field {...fields.middle} />
                            </Grid>
                            <Grid item xs={10} sm={4}>
                                <Field {...fields.last} />
                            </Grid>
                            <Grid item xs={4} sm={4}>
                                <Field {...fields.dl} />
                            </Grid>
                            <Grid item xs={4} sm={4}>
                                <Field {...fields.date_of_birth} />
                            </Grid>
                            <Grid item xs={12} sm={4}>
                                <Field {...fields.name_state} />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <Field {...fields.gender} />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <Field {...fields.marital_status} />
                            </Grid>
                        </Grid>
                    </React.Fragment>
                </ParentAccordion>
            </Collapse>
            <ParentAccordion title="Garaging Address">
                <React.Fragment>
                    <Grid container spacing={3}>
                        <Grid item xs={12} sm={12}>
                            <Field {...fields.address} />
                        </Grid>
                        <Grid item xs={10} sm={5}>
                            <Field {...fields.city} />
                        </Grid>
                        <Grid item xs={10} sm={5}>
                            <Field {...fields.garaging_state} />
                        </Grid>
                        <Grid item xs={4} sm={5}>
                            <Field {...fields.zip} />
                        </Grid>
                        <Grid item xs={7} sm={5}>
                            <FormControlLabel
                                className={classes.align}
                                control={
                                    <Checkbox
                                        checked={state.isMailing}
                                        onChange={() => { handleMailingAddress('isMailing', state.isMailing) }}
                                        color="primary"
                                    />
                                }
                                label="Mailing same as garaged"
                            />
                        </Grid>
                    </Grid>
                </React.Fragment>
            </ParentAccordion>
            <Collapse in={!state.isMailing}>
                <ParentAccordion title="Mailing Address">
                    <React.Fragment>
                        <Grid container spacing={3}>
                            <Grid item xs={12} sm={12}>
                                <Field {...fields.address} />
                            </Grid>
                            <Grid item xs={10} sm={5}>
                                <Field {...fields.city} />
                            </Grid>
                            <Grid item xs={10} sm={5}>
                                <Field {...fields.garaging_state} />
                            </Grid>
                            <Grid item xs={4} sm={5}>
                                <Field {...fields.zip} />
                            </Grid>
                        </Grid>
                    </React.Fragment>
                </ParentAccordion>
            </Collapse>
            <ParentAccordion title="Coverages">
                <React.Fragment>
                    <Grid container spacing={3}>
                        <Grid item xs={12} sm={12}>
                            <ChildAccordion
                                title="Full 21 Day Coverage"
                                subtitle="BI - $25K/$50K | PD - $15K | MP - $1K | Comp/Coll - $500 Deduct | Max $50K | ($253)"
                                description=""
                                descriptionTable="By clicking in the box above, I hereby acknowledge the coverage selected above.$25,000/$50,000 Bodily Injury(BI).$15,000 Property Damage (PD).$1,000 Medical Payments (MP)Collision and Comprehensive.$500 Deductible.Maximum Vehicle Value $50,000"
                                hasCheckBox={true}
                                isDisabled={true}
                                isChecked={true}
                            >
                                <span>sss</span>
                            </ChildAccordion>
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <ChildAccordion
                                title="Uninsured Motorist Coverage"
                                subtitle="UMBI - $25K/$50K | ($9)"
                                description="You have the legal right to purchase both Uninsured Motorist BI Coverage and Underinsured Motorist BI Coverage with the proposed automobile liability policy. 
                                            THESE COVERAGES PROTECT YOU, YOUR FAMILY, AND YOUR PASSENGERS. LIABILITY COVERAGE DOES NOT, 
                                            IN MOST CASES. Uninsured motorist insurance provides protection for bodily injuries caused by a negligent motorist who has no insurance. 
                                            Underinsured motorist coverage provides protection if the negligent motorist does not have enough liability insurance to pay for the injuries caused. 
                                            For a more detailed explanation of these coverages, refer to your policy. This policy will provide Uninsured/Underinsured coverage in the same amount as the policy's Bodily Injury Liability Limit, 
                                            unless you select a lower amount or not coverage, as stated in this notice. You have a right to purchase Uninsured Motorist coverage, 
                                            and Underinsured Motorist coverage in any amount from $25,000/$50,000 up to your policy's liability limit, or you may reject the coverage entirely. 
                                            Neither limit may exceed your liability coverage limits for Bodily Injury."
                                hasCheckBox={false}
                                hasRadioGroup={true}
                                isDisabled={false}
                                isChecked={false}
                            >
                                <span>sss</span>
                            </ChildAccordion>
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <ChildAccordion
                                title="Underinsured Motorist Coverage"
                                subtitle="UMBI - $25K/$50K | ($8)"
                                description="You have the legal right to purchase both Uninsured Motorist BI Coverage and Underinsured Motorist BI Coverage with the proposed automobile liability policy. 
                                            THESE COVERAGES PROTECT YOU, YOUR FAMILY, AND YOUR PASSENGERS. LIABILITY COVERAGE DOES NOT, 
                                            IN MOST CASES. Uninsured motorist insurance provides protection for bodily injuries caused by a negligent motorist who has no insurance. 
                                            Underinsured motorist coverage provides protection if the negligent motorist does not have enough liability insurance to pay for the injuries caused. 
                                            For a more detailed explanation of these coverages, refer to your policy. This policy will provide Uninsured/Underinsured coverage in the same amount as the policy's Bodily Injury Liability Limit, 
                                            unless you select a lower amount or not coverage, as stated in this notice. You have a right to purchase Uninsured Motorist coverage, 
                                            and Underinsured Motorist coverage in any amount from $25,000/$50,000 up to your policy's liability limit, or you may reject the coverage entirely. 
                                            Neither limit may exceed your liability coverage limits for Bodily Injury."
                                hasCheckBox={false}
                                hasRadioGroup={true}
                                isDisabled={false}
                                isChecked={false}
                            >
                                <span>sss</span>
                            </ChildAccordion>
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <ChildAccordion
                                title="Safety Equipment Coverage "
                                subtitle="$0 deductable Glass Coverage | ($9)"
                                description="By clicking in the box above, I hereby acknowledge the coverage selected above. 
                                        In consideration of an additional premium per vehicle, 
                                        the deductible provision of this policy as pertains to Comprehensive coverage does not aply to the repair of automobile safety equipment. 
                                        'Safety equipment' as referred to in this endorsement means the glass used in the windshield,
                                        doors and wndows and the glass or plastic or other material used in the light of your insured vehicle."
                                hasCheckBox={true}
                                isDisabled={false}
                                isChecked={false}
                            >
                                <span>sss</span>
                            </ChildAccordion>
                        </Grid>
                    </Grid>
                </React.Fragment>
            </ParentAccordion>
            <ParentAccordion title="Payment">
                <React.Fragment>
                    <Grid container spacing={3}>
                        <Grid item xs={12} sm={5}>
                            <FormControl className={classes.formControl}>
                                <TextField
                                    select
                                    variant="outlined"
                                    margin="dense"
                                    onChange={
                                        (e: React.ChangeEvent<{
                                            name?: string | undefined;
                                            value: unknown;
                                        }>) => {
                                            handleCreditCard(e.target.value);
                                        }
                                    }
                                    fullWidth
                                    SelectProps={{
                                        native: true,
                                    }}

                                >
                                    <option key={"dealer"} value={"dealer"}>
                                        Dealer
                                </option>
                                    <option key={"credit_card"} value={"credit_card"}>
                                        Credit Card
                                </option>
                                </TextField>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <Grid container spacing={3}>
                                <Grid item xs={12} sm={8} md={8}>
                                    {state.isCreditCard && (<CreditCardForm></CreditCardForm>)}
                                    {!state.isCreditCard && (<DealerForm></DealerForm>)}
                                </Grid>
                                <Grid item xs={12} sm={4} md={4}>
                                    <FormHelperText className={classes.total} variant="standard">
                                        Total: $253.00
                                </FormHelperText>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <Field {...fields.certification} />
                            <FormHelperText className={classes.text} variant="standard">
                                I certify that I have read and accept the <Link href="#">Terms and Conditions </Link>
                                and consent to transact using electronic communications, to receive
                                notices and disclosures electronically, and to utilize electronic
                                signatures in lieu of using paper documents.
                            </FormHelperText>
                        </Grid>
                    </Grid>
                </React.Fragment>
            </ParentAccordion>
        </>
    );
};
const GoForm = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid item xs={12} sm={4} style={{ display: 'contents', margin: 'auto' }}>
                <SideBar></SideBar>
            </Grid>
            <Grid item xs={12} sm={8}>
                <p style={{ textAlign: 'right', marginRight: '15px' }}> <a href="/">Logout</a></p>
                <Form
                    action="http://localhost:4351/api/contactus"
                    fields={fields}
                    render={() => (
                        <MainForm></MainForm>
                    )}
                />
                       <Copyright/>
            </Grid>
     
        </div>
    );
};
export default GoForm;
