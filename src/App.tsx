import React from 'react';
import { Switch, Route } from "react-router";
import GoForm from './container/GoForm';
import LogInForm from './container/LogInForm';
import ForgotPassword from './container/ForgotPassword';
import Bound from './container/Bound';

function App() {
  return (
    <Switch>
      <Route exact path="/" component={LogInForm}></Route>
      <Route path="/goUI" component={GoForm}></Route>
      <Route path="/restore" component={ForgotPassword}></Route>
      <Route path="/bound" component={Bound}></Route>
    </Switch>
  );
}

export default App;
